#Template Filippov Vladislav Yurevich 
#Email vlad.filippov.tmn@gmail.com
#Phone 8(861)203-54-44

### Dev tools
`Install node js https://nodejs.org/en/`

`Install gulp: npm install --global gulp`

`Install bower: npm install -g bower`

### Instal project

`npm i`

`bower install`

`Start project: gulp`

`Build project: gulp build`

### folder architecture

`app - workspace`

    `css - compiled scss`
    `html - template`
    `images - picture`
    `js - javascript`
    `libs - libraries`
    `sass - style site`
    
`dist - production version`